package me.duzhi.blog;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 * Copyright (c) 二月,28,2017 (ashang.peng@aliyun.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class Startup {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);
        WebAppContext webApp = new WebAppContext();
        webApp.setContextPath("/");
        // http://localhost:8080/hello/kongxx
        ServletContextHandler context1 = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context1.setContextPath("/");
        context1.setResourceBase(".");
        context1.setClassLoader(Thread.currentThread().getContextClassLoader());
        webApp.setWar("E:\\code\\project\\dz\\DUZHI_BLOG\\src\\main\\webapp");
        webApp.setClassLoader(Thread.currentThread().getContextClassLoader());
        ContextHandlerCollection contexts = new ContextHandlerCollection();
        contexts.setHandlers(new Handler[] { context1 });
        server.setHandler(contexts);
        webApp.setServer(server);
        webApp.start();

    }

}

package me.duzhi.blog.hanlder;

import com.jfinal.handler.Handler;
import me.duzhi.blog.hok.HokInvoke;
import me.duzhi.blog.hok.HokKit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ashang.peng@aliyun.com
 * @date 二月 14, 2017
 */

public class HoldHandler extends Handler {
    public static final String _ACTION_HANDLER = "_ACTION_HANDLER";

    @Override
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
        Object[] params = new Object[]{target,request,response,isHandled};
        HokInvoke.Message message = new HokInvoke.Message();
        message.setData(params);
        HokKit.invoke(message, new HokInvoke.Inv<Object>() {
            @Override
            public Object invoke(HokInvoke.Message message) {
                next.handle(target, request, response, isHandled);
                return null;
            }
        }, _ACTION_HANDLER);
    }
}

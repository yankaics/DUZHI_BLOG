package me.duzhi.blog.plugins.compress;

import io.jpress.core.render.JFreemarkerRender;
import io.jpress.message.Actions;
import io.jpress.message.Message;
import io.jpress.message.MessageListener;
import io.jpress.message.annotation.Listener;

/**
 * @author ashang.peng@aliyun.com
 * @date 一月 07, 2017
 */
@Listener(action = {Actions.JPRESS_STARTED})
public class CompressListener  implements MessageListener{
    @Override
    public void onMessage(Message message) {
        JFreemarkerRender.getConfiguration().setSharedVariable("assets", new CompressDirective());
    }
}
